"use strict";
const mongoose = require('mongoose');


const AssessmentSchema = new mongoose.Schema({
      name : String,
      time : String,
      locationId : Number,
      duration : Number,
      isExam : Boolean,
      weight : Number,
      moduleId : Number
});

mongoose.model('Assessment', AssessmentSchema);
module.exports = mongoose.model('Assessment');

