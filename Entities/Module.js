"use strict";
const mongoose = require('mongoose');


const ModuleSchema = new mongoose.Schema({
    name: String,
    code: String,
    credits: Number,
    level: Number,
    studentcount: Number
});

mongoose.model('Module', ModuleSchema);
module.exports = mongoose.model('Module');

