"use strict";
const mongoose = require('mongoose');


const StudentSchema = new mongoose.Schema({    
    firstName : String,
    lastName : String,    
    studentId : Number
});

mongoose.model('Student', StudentSchema);
module.exports = mongoose.model('Student');

