"use strict";
const mongoose = require('mongoose');


const Student_ModuleSchema = new mongoose.Schema({
    studentId : Number,
    moduleId : Number
    
});

mongoose.model('Student_Module', Student_ModuleSchema);
module.exports = mongoose.model('Student_Module');

