"use strict";
const mongoose = require('mongoose');


const LocationSchema = new mongoose.Schema({
        className : String,
        Building : Number
});

mongoose.model('Location', LocationSchema);
module.exports = mongoose.model('Location');

