"use strict";
const express = require('express');
const app = express();

var bodyParser = require('body-parser');
const assessmentService = require('./Services/AssessmentService');
const  moduleService = require('./Services/ModuleService');
const student_moduleService = require('./Services/Student_moduleService');
const locationService = require('./Services/LocationService');
const studentService = require('./Services/StudentService');


app.use(bodyParser.urlencoded({extended: 'false'}));
app.use(bodyParser.json());

app.use('/assessment', assessmentService);
app.use('/module', moduleService);
app.use('/student/module', student_moduleService);
app.use('/location', locationService);
app.use('/student', studentService);

app.listen(8080, function(){
    console.log("listening on port 8080");
})

module.exports = app;
