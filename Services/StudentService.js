const studentDb = require('../Database/StudentDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Student = require('../Entities/Student');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const students = await studentDb.getStudent();     
     if(students != null){
        console.log(' Students retrieved!');
        res.send(students);        
     }else{
        console.log(' Students not retrieved');
        res.send(' Students not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get students');
         console.log('Exception: Cannot get students');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientStudent = req.body;                
        const result = await studentDb.addStudent(clientStudent);
        if (result == 1)
        {
            res.send("student Added!");
            console.log("student added successfully");
        }else{
            res.send("student not added!");
            console.log("student not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: student not added");
        res.send('Exception: student not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const stringId = req.params;     
        const id = parseInt(stringId.id);
        const clientStudent = req.body;                        
        const result = await studentDb.updateStudent(clientStudent,id);        
        if(result != null)
        {
            res.send('student updated');
            console.log('student updated');
        }else{
            console.log('Student entry not found');
            res.send('Student entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: student not updated');
        res.send('Exception: student not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const stringId = req.params;
        const id = parseInt(stringId.id);
        const result = await studentDb.deleteStudent(id);        
        if(result == 1)
        {
            res.send('Student deleted successfully');
            console.log('Student deleted successfully');
        }else{
            console.log('Student not deleted');
            res.send('Student not deleted');
        }
    }catch(error){
        console.error(error);
        console.log('Controller Exception: Student not deleted ');
        res.send('Exception: Student not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await studentDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Student found!');
        }else{
            res.send('Student not found!');
            console.log('Student not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get student by id');
        res.send(' Exception: Cannot get student');
    }
})




module.exports = router;