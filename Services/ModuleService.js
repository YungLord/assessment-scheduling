const moduleDb = require('../Database/ModuleDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Module = require('../Entities/Module');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const modules = await moduleDb.getModule();
     if(modules != null){
        console.log(' Modules retrieved!');
        res.send(modules);
     }else{
        console.log(' Modules not retrieved');
        res.send(' Modules not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get modules');
         console.log('Exception: Cannot get modules');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientModule = req.body;                
        console.log(clientModule);
        
        const result = await moduleDb.addModule(clientModule);
        if (result == 1)
        {
            res.send("module Added!");
            console.log("module added successfully");
        }else{
            res.send("module not added!");
            console.log("module not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: module not added");
        res.send('Exception: module not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const stringId = req.params;     
        const id = parseInt(stringId.id);
        const clientModule = req.body;                        
        const result = await moduleDb.updateModule(clientModule,id);        
        if(result != null)
        {
            res.send('module updated');
            console.log('module updated');
        }else{
            console.log('Module entry not found');
            res.send('Module entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: module not updated');
        res.send('Exception: module not updated');
    }
})

router.delete('/delete/:id', async(req,res)=>{
    try{    
        const stringId = req.params;
        const id = parseInt(stringId.id);
        const result = await moduleDb.deleteModule(id);        
        if(result == 1)
        {
            res.send('Module deleted successfully');
            console.log('Module deleted successfully');
        }else{
            console.log('Module not deleted');
            res.send('Module not deleted');
        }
    }catch(error){
        console.error(error);
        console.log('Controller Exception: Module not deleted ');
        res.send('Exception: Module not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await moduleDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Module found!');
        }else{
            res.send('Module not found!');
            console.log('Module not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get module by id');
        res.send(' Exception: Cannot get module');
    }
})



module.exports = router;