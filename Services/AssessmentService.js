const assessmentDb = require('../Database/AssessmentDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Assessment = require('../Entities/Assessment');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const assessments = await assessmentDb.getAssessment();     
     if(assessments != null){
        console.log(' Assessments retrieved!');
        res.send(assessments);        
     }else{
        console.log(' Assessments not retrieved');
        res.send(' Assessments not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get assessments');
         console.log('Exception: Cannot get assessments');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientAssessment = req.body;                
        const result = await assessmentDb.addAssessment(clientAssessment);
        if (result == 1)
        {
            res.send("assessment Added!");
            console.log("assessment added successfully");
        }else{
            res.send("assessment not added!");
            console.log("assessment not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: assessment not added");
        res.send('Exception: assessment not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const stringId = req.params;     
        const id = parseInt(stringId.id);
        const clientAssessment = req.body;                        
        const result = await assessmentDb.updateAssessment(clientAssessment,id);        
        if(result != null)
        {
            res.send('assessment updated');
            console.log('assessment updated');
        }else{
            console.log('Assessment entry not found');
            res.send('Assessment entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: assessment not updated');
        res.send('Exception: assessment not updated');
    }
})

router.delete('/delete/:id', async(req,res)=>{
    try{    
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);                         
        const result = await assessmentDb.deleteAssessment(o_id);        
        if(result == 1)
        {
            res.send('Assessment deleted successfully');
            console.log('Assessment deleted successfully');
        }else{
            console.log('Assessment not deleted');
            res.send('Assessment not deleted');
        }
    }catch(error){
        console.error(error);
        console.log('Controller Exception: Assessment not deleted ');
        res.send('Exception: Assessment not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await assessmentDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Assessment found!');
        }else{
            res.send('Assessment not found!');
            console.log('Assessment not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get assessment by id');
        res.send(' Exception: Cannot get assessment');
    }
})



module.exports = router;