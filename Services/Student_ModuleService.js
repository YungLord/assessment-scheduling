const student_moduleDb = require('../Database/Student_ModuleDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Student_Module = require('../Entities/Student_Module');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const student_modules = await student_moduleDb.getStudent_Module();     
     if(student_modules != null){
        console.log(' Student_Modules retrieved!');
        res.send(student_modules);        
     }else{
        console.log(' Student_Modules not retrieved');
        res.send(' Student_Modules not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get student_modules');
         console.log('Exception: Cannot get student_modules');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientStudent_Module = req.body;                
        const result = await student_moduleDb.addStudent_Module(clientStudent_Module);
        if (result == 1)
        {
            res.send("student_module Added!");
            console.log("student_module added successfully");
        }else{
            res.send("student_module not added!");
            console.log("student_module not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: student_module not added");
        res.send('Exception: student_module not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const stringId = req.params;     
        const id = parseInt(stringId.id);
        const clientStudent_Module = req.body;                        
        const result = await student_moduleDb.updateStudent_Module(clientStudent_Module,id);        
        if(result != null)
        {
            res.send('student_module updated');
            console.log('student_module updated');
        }else{
            console.log('Student_Module entry not found');
            res.send('Student_Module entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: student_module not updated');
        res.send('Exception: student_module not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const stringId = req.params;
        const id = parseInt(stringId.id);
        const result = await student_moduleDb.deleteStudent_Module(id);        
        if(result == 1)
        {
            res.send('Student_Module deleted successfully');
            console.log('Student_Module deleted successfully');
        }else{
            console.log('Student_Module not deleted');
            res.send('Student_Module not deleted');
        }
    }catch(error){
        console.error(error);
        console.log('Controller Exception: Student_Module not deleted ');
        res.send('Exception: Student_Module not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await student_moduleDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Student_Module found!');
        }else{
            res.send('Student_Module not found!');
            console.log('Student_Module not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get student_module by id');
        res.send(' Exception: Cannot get student_module');
    }
})



module.exports = router;