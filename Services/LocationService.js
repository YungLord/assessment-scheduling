const locationDb = require('../Database/LocationDb');
const express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs');
const Location = require('../Entities/Location');
const ObjectId = require('mongodb').ObjectId;

router.get('/all', async  (req, res)=> {   
    try{        
     const locations = await locationDb.getLocation();     
     if(locations != null){
        console.log(' Locations retrieved!');
        res.send(locations);        
     }else{
        console.log(' Locations not retrieved');
        res.send(' Locations not retrieved');
     }
     }catch(err)
     {
         console.error(err);
         console.log('Controller Exception: Cannot get locations');
         console.log('Exception: Cannot get locations');

     }
})

router.post('/add',async (req,res)=>{
    try{
        const clientLocation = req.body;     
        console.log(clientLocation);
                     
        const result = await locationDb.addLocation(clientLocation);
        if (result == 1)
        {
            res.send("location Added!");
            console.log("location added successfully");
        }else{
            res.send("location not added!");
            console.log("location not added");
        }
    }catch(err)
    {
        console.error(err);
        console.log("Exception: location not added");
        res.send('Exception: location not added');
    }    
})

router.put('/:id',async (req,res)=>{    
    try{   
        const stringId = req.params;     
        const id = parseInt(stringId.id);
        const clientLocation = req.body;                        
        const result = await locationDb.updateLocation(clientLocation,id);        
        if(result != null)
        {
            res.send('location updated');
            console.log('location updated');
        }else{
            console.log('Location entry not found');
            res.send('Location entry not found');
        }        
    }catch(error){
        console.error(error);
        console.log('Controller Exception: location not updated');
        res.send('Exception: location not updated');
    }
})

router.delete('/:id', async(req,res)=>{
    try{    
        const stringId = req.params;
        const id = parseInt(stringId.id);
        const result = await locationDb.deleteLocation(id);        
        if(result == 1)
        {
            res.send('Location deleted successfully');
            console.log('Location deleted successfully');
        }else{
            console.log('Location not deleted');
            res.send('Location not deleted');
        }
    }catch(error){
        console.error(error);
        console.log('Controller Exception: Location not deleted ');
        res.send('Exception: Location not deleted');
    }
})

router.get('/:_id', async (req,res)=>{
    try{                                        
        const JSONId = req.params;                
        const id = JSONId._id        
        const o_id = new ObjectId(id);  
        const result = await locationDb.getById(o_id);                ;
        if(result != null)
        {
            res.send(result);
            console.log('Location found!');
        }else{
            res.send('Location not found!');
            console.log('Location not found!');
        }
    }catch(error)
    {        
        console.error(error);
        console.log('Controller Exception: Cannot get location by id');
        res.send(' Exception: Cannot get location');
    }
})



module.exports = router;