"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_location';
const Location = require('../Entities/Location');




let location = {
    id: 6,
    name : "babe"
}
//let newLocation = new Location(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initLocationDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const locationTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Location Collection live");              
        });    
        return locationTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Location collection')
    }
 
}
//initLocationDb();

const addLocation = async ( location )=>{   
    try{
        const locationTable = await initLocationDb();        
        const result = await locationTable.insertOne(location);  
        if (result.insertedCount == 1){            
            console.log("1 Location added successfully");            
            return result.insertedCount;
        }else{
            console.log("Location not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Location not added");
        console.error(error);
    }    
}
//addLocation(location);

module.exports.getLocation = async ()=>{   
    try{
        const locationTable = await initLocationDb();
        const locations = await locationTable.find().toArray();
        console.log("Locations retrieved!");          
        return locations;
    }catch(error)
    {
        console.log("Database Exception: cannot get");
        console.error(error);
    }
}
// getLocation();

const getById = async (id)=>{           
    try{            
        const locationTable = await initLocationDb();        
        const newLocation = await locationTable.findOne({_id:id});  
        console.log(id);
        if ( newLocation != null )
            {
                console.log("location was found");                
                return newLocation;
            }
            else{
                console.log("location was not found");
                return newLocation;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateLocation = async (location,id)=>{           
    try{
        location.id = id;
        const locationTable = await initLocationDb();
        const newLocation = await locationTable.findOneAndUpdate(
            {id: id},
            {$set: location},
            {upsert: false});

            if(newLocation.value != null)
            {
                console.log("location updated");
                return newLocation;
            }else{
                console.log("location was not updated");
                return newLocation;
            }        
    }catch(error){
        console.log("Database Exception: location was not updated");
        console.error(error);
    }       
}
//updateLocation(newLocation,id);

const deleteLocation = async (id)=>{               
    try{
        const locationTable = await initLocationDb();
        const newLocation = await locationTable.deleteOne({_id : id})
         .then(result =>{
            if (result.deletedCount == 0) {
                console.log('No locations to delete');
                return 0;
              }
            else{
                console.log('location deleted');
                return 1;
            }              
         })        
    }catch(error){        
        console.log("Database Exception: location was not deleted");
        console.error(error);
    }       
}
//deleteLocation(id);



module.exports.getById = getById;
module.exports.initLocationDb = initLocationDb;
module.exports.addLocation = addLocation;
module.exports.deleteLocation = deleteLocation;
module.exports.updateLocation = updateLocation;
