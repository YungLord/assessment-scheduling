"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_student';
const Student = require('../Entities/Student');

let student = {
    id: 6,
    name : "babe"
}
//let newStudent = new Student(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 

const initStudentDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const studentTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Student Collection live");              
        });    
        return studentTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Student collection')
    }
 
}
//initStudentDb();

const addStudent = async ( student )=>{   
    try{
        const studentTable = await initStudentDb();        
        const result = await studentTable.insertOne(student);  
        if (result.insertedCount == 1){            
            console.log("1 Student added successfully");            
            return result.insertedCount;
        }else{
            console.log("Student not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Student not added");
        console.error(error);
    }    
}
//addStudent(student);

module.exports.getStudent = async ()=>{   
    try{
        const studentTable = await initStudentDb();
        const students = await studentTable.find().toArray();
        console.log("Students retrieved!");          
        return students;
    }catch(error)
    {
        console.log("Database Exception: cannot get");
        console.error(error);
    }
}
 //getStudent();

const getById = async (id)=>{           
    try{            
        const studentTable = await initStudentDb();        
        const newStudent = await studentTable.findOne({_id:id});  
        
        if ( newStudent != null )
            {
                console.log("student was found");                
                return newStudent;
            }
            else{
                console.log("student was not found");
                return newStudent;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateStudent = async (student,id)=>{           
    try{
        student.id = id;
        const studentTable = await initStudentDb();
        const newStudent = await studentTable.findOneAndUpdate(
            {id: id},
            {$set: student},
            {upsert: false});

            if(newStudent.value != null)
            {
                console.log("student updated");
                return newStudent;
            }else{
                console.log("student was not updated");
                return newStudent;
            }        
    }catch(error){
        console.log("Database Exception: student was not updated");
        console.error(error);
    }       
}
//updateStudent(newStudent,id);

const deleteStudent = async (id)=>{               
    try{
        const studentTable = await initStudentDb();        
        const result = await studentTable.deleteOne({_id : id});
        console.log(result);
        
            if (result.deletedCount == 1) {
                console.log('student deleted');
                return 1;
                
              }
            else{
                console.log('No students to delete');
                return 0;
            }                       
    }catch(error){        
        console.log("Database Exception: student was not deleted");
        console.error(error);
    }       
}
//deleteStudent(id);


module.exports.getById = getById;
module.exports.initStudentDb = initStudentDb;
module.exports.addStudent = addStudent;
module.exports.deleteStudent = deleteStudent;
module.exports.updateStudent = updateStudent;