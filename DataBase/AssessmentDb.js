"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_assessment';
const Assessment = require('../Entities/Assessment');

let assessment = {
    id: 6,
    name : "babe"
}
//let newAssessment = new Assessment(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initAssessmentDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const assessmentTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Assessment Collection live");              
        });    
        return assessmentTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Assessment collection')
    }
 
}
//initAssessmentDb();

const addAssessment = async ( assessment )=>{   
    try{
        const assessmentTable = await initAssessmentDb();        
        const result = await assessmentTable.insertOne(assessment);  
        if (result.insertedCount == 1){            
            console.log("1 Assessment added successfully");            
            return result.insertedCount;
        }else{
            console.log("Assessment not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Assessment not added");
        console.error(error);
    }    
}
//addAssessment(assessment);

module.exports.getAssessment = async ()=>{   
    try{
        const assessmentTable = await initAssessmentDb();
        const assessments = await assessmentTable.find().toArray();
        console.log("Assessments retrieved!");          
        return assessments;
    }catch(error)
    {
        console.log("Database Exception: cannot get Assessments");
        console.error(error);
    }
}
// getAssessment();

const getById = async (id)=>{           
    try{            
        const assessmentTable = await initAssessmentDb();        
        const newAssessment = await assessmentTable.findOne({_id:id});  
        console.log(id);
        if ( newAssessment != null )
            {
                console.log("assessment was found");                
                return newAssessment;
            }
            else{
                console.log("assessment was not found");
                return newAssessment;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateAssessment = async (assessment,id)=>{           
    try{
        assessment.id = id;
        const assessmentTable = await initAssessmentDb();
        const newAssessment = await assessmentTable.findOneAndUpdate(
            {id: id},
            {$set: assessment},
            {upsert: false});

            if(newAssessment.value != null)
            {
                console.log("assessment updated");
                return newAssessment;
            }else{
                console.log("assessment was not updated");
                return newAssessment;
            }        
    }catch(error){
        console.log("Database Exception: assessment was not updated");
        console.error(error);
    }       
}
//updateAssessment(newAssessment,id);

const deleteAssessment = async (id)=>{               
    try{
        const assessmentTable = await initAssessmentDb();
        const result = await assessmentTable.deleteOne({_id : id});
        console.log(result);                 
            if (result.deletedCount == 1) {
                console.log('assessment deleted');
                return 1;             
              }
            else   {              
                console.log('No assessments to delete');
                return 0;
            }              
         
    }catch(error){        
        console.log("Database Exception: assessment was not deleted");
        console.error(error);
    }       
}
//deleteAssessment(id);


module.exports.getById = getById;
module.exports.initAssessmentDb = initAssessmentDb;
module.exports.addAssessment = addAssessment;
module.exports.deleteAssessment = deleteAssessment;
module.exports.updateAssessment = updateAssessment;
