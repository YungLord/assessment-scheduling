"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_module';
const Module = require('../Entities/Module');


//let newModule = new Module(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initModuleDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const moduleTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Module Collection live");              
        });    
        return moduleTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Module collection')
    }
 
}
//initModuleDb();

const addModule = async ( module )=>{   
    try{
        const moduleTable = await initModuleDb();        
        const result = await moduleTable.insertOne(module);  
        if (result.insertedCount == 1){            
            console.log("1 Module added successfully");            
            return result.insertedCount;
        }else{
            console.log("Module not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Module not added");
        console.error(error);
    }    
}
//addModule(module);

module.exports.getModule = async ()=>{   
    try{
        const moduleTable = await initModuleDb();
        const modules = await moduleTable.find().toArray();
        console.log("Modules retrieved!");          
        return modules;
    }catch(error)
    {
        console.log("Database Exception: cannot get Modules");
        console.error(error);
    }
}
// getModule();

const getById = async (id)=>{           
    try{            
        const moduleTable = await initModuleDb();        
        const newModule = await moduleTable.findOne({_id:id});  
        console.log(id);
        if ( newModule != null )
            {
                console.log("module was found");                
                return newModule;
            }
            else{
                console.log("module was not found");
                return newModule;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateModule = async (module,id)=>{           
    try{
        module.id = id;
        const moduleTable = await initModuleDb();
        const newModule = await moduleTable.findOneAndUpdate(
            {id: id},
            {$set: module},
            {upsert: false});

            if(newModule.value != null)
            {
                console.log("module updated");
                return newModule;
            }else{
                console.log("module was not updated");
                return newModule;
            }        
    }catch(error){
        console.log("Database Exception: module was not updated");
        console.error(error);
    }       
}
//updateModule(newModule,id);

const deleteModule = async (id)=>{               
    try{
        const moduleTable = await initModuleDb();
        const newModule = await moduleTable.deleteOne({_id : id})
         .then(result =>{
            if (result.deletedCount == 0) {
                console.log('No modules to delete');
                return 0;
              }
            else{
                console.log('module deleted');
                return 1;
            }              
         })        
    }catch(error){        
        console.log("Database Exception: module was not deleted");
        console.error(error);
    }       
}
//deleteModule(id);


module.exports.getById = getById;
module.exports.initModuleDb = initModuleDb;
module.exports.addModule = addModule;
module.exports.deleteModule = deleteModule;
module.exports.updateModule = updateModule;
