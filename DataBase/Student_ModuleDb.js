"use strict";
const con = require('./MongoDb.js');
const TABLE_NAME = 'Yung_student_module';
const Student_Module = require('../Entities/Student_Module');

let student_module = {
    id: 6,
    name : "babe"
}

//let newStudent_Module = new Student_Module(3,'Mark','Doe','JohnnyD','Doeboy','markdoe@gmail.com');
let id = 3; 


const initStudent_ModuleDb = async ()=>{    
    try{
        const client = await con.connectDb();
        const student_moduleTable = client.collection(TABLE_NAME,function(error,response){
            if(error)
            {
                throw error;
            }
            console.log("Student_Module Collection live");              
        });    
        return student_moduleTable;
    }catch(error)
    {
        console.error(error);
        console.log('Database Exception: cannot connect to Student_Module collection')
    }
 
}
//initStudent_ModuleDb();

const addStudent_Module = async ( student_module )=>{   
    try{
        const student_moduleTable = await initStudent_ModuleDb();        
        const result = await student_moduleTable.insertOne(student_module);  
        if (result.insertedCount == 1){            
            console.log("1 Student_Module added successfully");            
            return result.insertedCount;
        }else{
            console.log("Student_Module not added");
            return result.insertedCount;
        }        
    }catch(error)
    {
        console.log("Database Exception: Student_Module not added");
        console.error(error);
    }    
}
//addStudent_Module(student_module);

module.exports.getStudent_Module = async ()=>{   
    try{
        const student_moduleTable = await initStudent_ModuleDb();
        const student_modules = await student_moduleTable.find().toArray();
        console.log("Student_Modules retrieved!");          
        return student_modules;
    }catch(error)
    {
        console.log("Database Exception: cannot get");
        console.error(error);
    }
}
// getStudent_Module();

const getById = async (id)=>{           
    try{            
        const student_moduleTable = await initStudent_ModuleDb();        
        const newStudent_Module = await student_moduleTable.findOne({_id:id});  
        console.log(id);
        if ( newStudent_Module != null )
            {
                console.log("student_module was found");                
                return newStudent_Module;
            }
            else{
                console.log("student_module was not found");
                return newStudent_Module;
            }
    }catch(error){
        console.log("Database Exception: cannot get by id");
        console.error(error);
    }       
}
//getById(id);

const updateStudent_Module = async (student_module,id)=>{           
    try{
        student_module.id = id;
        const student_moduleTable = await initStudent_ModuleDb();
        const newStudent_Module = await student_moduleTable.findOneAndUpdate(
            {id: id},
            {$set: student_module},
            {upsert: false});

            if(newStudent_Module.value != null)
            {
                console.log("student_module updated");
                return newStudent_Module;
            }else{
                console.log("student_module was not updated");
                return newStudent_Module;
            }        
    }catch(error){
        console.log("Database Exception: student_module was not updated");
        console.error(error);
    }       
}
//updateStudent_Module(newStudent_Module,id);

const deleteStudent_Module = async (id)=>{               
    try{
        const student_moduleTable = await initStudent_ModuleDb();
        const newStudent_Module = await student_moduleTable.deleteOne({_id : id})
         .then(result =>{
            if (result.deletedCount == 0) {
                console.log('No student_modules to delete');
                return 0;
              }
            else{
                console.log('student_module deleted');
                return 1;
            }              
         })        
    }catch(error){        
        console.log("Database Exception: student_module was not deleted");
        console.error(error);
    }       
}
//deleteStudent_Module(id);



module.exports.getById = getById;
module.exports.initStudent_ModuleDb = initStudent_ModuleDb;
module.exports.addStudent_Module = addStudent_Module;
module.exports.deleteStudent_Module = deleteStudent_Module;
module.exports.updateStudent_Module = updateStudent_Module;
